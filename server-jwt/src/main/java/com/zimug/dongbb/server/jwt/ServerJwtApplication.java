package com.zimug.dongbb.server.jwt;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages={"com.zimug"})
@MapperScan(basePackages = {"com.zimug.dongbb.**.mapper"})
public class ServerJwtApplication {

	public static void main(String[] args) {
		SpringApplication.run(ServerJwtApplication.class, args);
	}

}
