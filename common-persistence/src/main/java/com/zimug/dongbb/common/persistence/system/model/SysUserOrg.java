package com.zimug.dongbb.common.persistence.system.model;

import com.zimug.dongbb.common.persistence.auto.model.SysUser;

public class SysUserOrg extends SysUser {

  private String orgName;

  public String getOrgName() {
    return orgName;
  }
  public void setOrgName(String orgName) {
    this.orgName = orgName;
  }
}
